#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <stdbool.h>
#include <errno.h>
  
#define PORT 6789
#define MAXLINE 1024 
#define STDIN 0

const char *clientFiles[32];
int totalClientFiles = 0;
// char *serverReceivingPort = "6789";
	
int max(int first, int second) {
	if(first > second)
		return first;

	return second;
}

void printFiles() {
	int i;

	for(i = 0; i < totalClientFiles; i++) {
		write(1, clientFiles[i], strlen(clientFiles[i]));
	}
}

int main(int argc, char *argv[]) {
	char *firstToken, *secondToken;
	int sockfd;
	char buffer[MAXLINE];
	int n, len;
	fd_set readFds;
	int maxFd;
	char *msg;
	char* userInput;
	struct sockaddr_in servaddr;
	char *heartbeatPort, *broadcastPortY, *clientPortM;

	if(argc != 4) {
		msg = "USAGE: client heartbeatPortX broadcastPortY clientPortM\n";
		write(1, msg, strlen(msg));
		exit(1);
	}
	heartbeatPort = argv[1];
	broadcastPortY = argv[2];
	clientPortM = argv[3];

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORT);
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		msg = "Socket creation failed."; 
		write(2, msg, sizeof(msg));
		exit(2);
	}

	FD_ZERO(&readFds);

	maxFd = max(sockfd, STDIN);

	for(;;) {
		FD_SET(sockfd, &readFds);
		FD_SET(STDIN, &readFds);

		select(maxFd + 1, &readFds, NULL, NULL, NULL);

		if(FD_ISSET(sockfd, &readFds)) {
			n = recvfrom(sockfd, (char *)buffer, MAXLINE,  
					0, (struct sockaddr *) &servaddr, 
					&len);
			buffer[n] = '\0';

			// if(strcmp(strtok(buffer, " "), "RECEIVING")) {
			// 	serverReceivingPort = strtok(buffer, "\0 ");
			// }

			clientFiles[totalClientFiles] = malloc(n);
			strncpy((char *)clientFiles[totalClientFiles], buffer, n);

			// Reset the buffer.
			memset(buffer, 0, MAXLINE);

			msg = "The file was successfully downloaded.\n";
			write(1, msg, strlen(msg));

			totalClientFiles++;

			printFiles();
		} else if(FD_ISSET(STDIN, &readFds)) {
			n = read(STDIN, buffer, sizeof(buffer));
			buffer[n] = '\0';

			userInput = malloc(n);
			strncpy(userInput, buffer, n);

			firstToken = strtok(buffer, " ");
			secondToken = strtok(NULL, " \n");

			if(strcmp(firstToken, "download") == 0) {
				sendto(sockfd, (char *)userInput, strlen(userInput), 0,
						(const struct sockaddr *)&servaddr, sizeof servaddr);

			} else if(strcmp(firstToken, "upload") == 0) {
				sendto(sockfd, (char *)userInput, strlen(userInput), 0,
						(const struct sockaddr *)&servaddr, sizeof servaddr);

			} else {
				msg = "The specified command was not found.";
				write(1, msg, strlen(msg));
			}
		}
	}
}
