#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <stdbool.h>
#include <errno.h>

#define RECEIVE_PORT 6789 
#define MAXLINE 1024 
#define FILE_NOT_FOUND -1

// struct sockaddr_in clients[20];
// int cliNum = 0;
int sockfd;

int findTheFile(char *file, int serverTotalFiles, const char *serverFiles[]);
// void sendHeartbeat() {
// 	char *message = "RECEIVING 6789";
// 	int i;
// 	struct sockaddr_in client;

// 	for(i = 0; i < cliNum; i++) {
// 		client = clients[i];

// 		int bytes = sendto(sockfd, message, sizeof(message), 0,
// 				(struct sockaddr*)&client,sizeof(client));
    
//    		if (bytes < 0) {
//     		write(2, "Error in sending heartbeat!\n", sizeof("Error in sending heartbeat!\n"));
// 		}
// 	}

// 	signal(SIGALRM, sendHeartbeat);
// 	alarm(1);
// }

int main(int argc, char * argv[]) {
	const char *serverFiles[32];
	char buffer[MAXLINE];
	int len, n;
	struct sockaddr_in servaddr, cliaddr;
	fd_set readFds;
	int serverTotalFiles = 0;
	char *msg;
	int fileIndex;
	char *secondToken;
	char *firstToken;
	char *heartbeatPortX;
	int yes = 1;

	if(argc != 2) {
		msg = "USAGE: server heartbeatPortX\n";
		write(1, msg, strlen(msg));
		
		exit(1);
	}
	heartbeatPortX = argv[1];

	memset(&servaddr, 0, sizeof(servaddr));
	memset(&cliaddr, 0, sizeof(cliaddr));

	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(RECEIVE_PORT);
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		msg = "Socket creation failed.\n";
		write(2, msg, sizeof(msg));
		exit(2);
	}

	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

	if(bind(sockfd, (const struct sockaddr *)&servaddr,  
			sizeof(servaddr)) < 0) { 
		perror("Bind failed.\n");
		exit(3);
	}

	FD_ZERO(&readFds);

	// signal(SIGALRM, sendHeartbeat);
	// alarm(1);

	for(;;) {
		FD_SET(sockfd, &readFds);

		select(sockfd + 1, &readFds, NULL, NULL, NULL);

		if(FD_ISSET(sockfd, &readFds)) {
			n = recvfrom(sockfd, (char*)buffer, MAXLINE,
					0, (struct sockaddr *)&cliaddr, &len);
			buffer[n] = '\0';
			// clients[cliNum] = cliaddr;
			// cliNum++;

			firstToken = strtok(buffer, " ");
			secondToken = strtok(NULL, " ");
			
			if(strcmp(firstToken, "download") == 0) {
				fileIndex = findTheFile(secondToken, serverTotalFiles, serverFiles);

				if(fileIndex == FILE_NOT_FOUND) {
					msg = "The file was not found\n";
					write(2, msg, strlen(msg));
				} else {
					msg = "The file was successfully sent!\n";
					write(1, msg, strlen(msg));

					sendto(sockfd, (const char *)secondToken, strlen(secondToken), 0,
							(const struct sockaddr *)&cliaddr, len);
				}
			} else if(strcmp(firstToken, "upload") == 0) {
				fileIndex = findTheFile(secondToken, serverTotalFiles, serverFiles);

				if(fileIndex != FILE_NOT_FOUND) {
					msg = "The specified file already exists on the server.\n";

					write(2, msg, strlen(msg));

					continue;
				}

				serverFiles[serverTotalFiles] = secondToken;
				serverTotalFiles++;

				msg = "The file was successfully uploaded.\n";
				write(1, msg, strlen(msg));
			} else {
				msg = "The specified command was not found\n";

				write(2, msg, strlen(msg));
			}
		}
	}

	return 0; 
} 

int findTheFile(char *file, int serverTotalFiles, const char *serverFiles[]) {
	int i;

	for(i = 0; i < serverTotalFiles; i++) {
		if(strcmp(serverFiles[i], file)) {
			return i;
		}
	}

	return FILE_NOT_FOUND;
}
